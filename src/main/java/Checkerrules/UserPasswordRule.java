package Checkerrules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.User;

public class UserPasswordRule implements ICanCheckRule<User> {

	public CheckResult checkRule(User entity) {
		
		if(entity.getPassword()==null){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getPassword().equals("")){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getPassword().equals("123456")){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getPassword().equals("abcdef")){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getPassword().equals("qwerty")){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getPassword().length()<7){
			return new CheckResult("Wrong", RuleResult.Error);
		}
			
		return new CheckResult("Fine", RuleResult.Ok);
	}

}
