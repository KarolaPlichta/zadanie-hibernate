package Repository;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import UnitOfWork.UnitOfWork;
import UnitOfWork.UnitOfWorkRepository;
import domain.Entity;
import domain.PhoneNumber;

public class HsqlPhoneNumberRepository implements PhoneNumberRepository, UnitOfWorkRepository{
	
	private Query query;
	private Session session;
	private UnitOfWork unitOfWork;

	protected HsqlPhoneNumberRepository(Session session, UnitOfWork unitOfWork) {
		this.session = session;
		this.unitOfWork = unitOfWork;
	}

	public PhoneNumber withId(int id) {
		PhoneNumber number = new PhoneNumber();
		number = session.get(PhoneNumber.class, id);
		return number;
	}

	@SuppressWarnings("unchecked")
	public List<PhoneNumber> selectAll() {
		String selectAll = "FROM phonenumber ";
        query=session.createQuery(selectAll);
        return query.list();
	}
	
	public void add(PhoneNumber entity) {
		session.save(entity);
	}

	public void delete(PhoneNumber entity) {
		session.delete(entity);
		
	}

	public void modify(PhoneNumber entity) {
		session.update(entity);

	}

	public int count() {
		int countPhoneNumber =0;
		countPhoneNumber = selectAll().size();
		return countPhoneNumber;
	}

	@SuppressWarnings("unchecked")
	public List<PhoneNumber> withCountryPrefix(String prefix) {
		String withCountryPrefix = "FROM phonenumber WHERE countyPrefix = :prefix";
        query=session.createQuery(withCountryPrefix);
        query.setParameter("prefix",prefix);
        return query.list();
	}

	public PhoneNumber withNumber(String number) {
		PhoneNumber phoneNumber = new PhoneNumber();
		  phoneNumber = session.get(PhoneNumber.class, number);
		return phoneNumber;
	}

	@SuppressWarnings("unchecked")
	public List<PhoneNumber> withCountryAndCityPrefix(String countryPrefix, String cityPrefix) {
		String withCountryPrefix = "FROM phonenumber WHERE countyPrefix = :countryPrefix AND cityPrefix =: cityPrefix";
        query=session.createQuery(withCountryPrefix);
        query.setParameter("countryPrefix",countryPrefix);
        query.setParameter("cityPrefix",cityPrefix);
        return query.list();
		
	}
	
	public void persistAdd(Entity entity) {
		unitOfWork.markAsNew(entity, this);
	}

	public void persistDelete(Entity entity) {
		unitOfWork.markAsDeleted(entity, this);
	}

	public void persistUpdate(Entity entity) {
		unitOfWork.markAsChanged(entity, this);
		
	}



}
