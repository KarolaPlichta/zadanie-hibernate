package Repository;

import java.util.List;

import domain.Address;

public interface AddressRepository extends IRepository<Address> {
	
	public List<Address> withCountryId(int id);
	public List<Address> withZipCode(String zipCode);
	public List<Address> withCountryIdAndCity(int id, String city);

}
