package Cache;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Cache{
	
	private Map<String, ArrayList> items;
	
	
	private Cache(){
		this.items = new HashMap<String, ArrayList>();
	}
	
	private static Cache instance;

	public synchronized static Cache getInstance(){
		if (instance == null){
				instance = new Cache();
		}
		return instance;
		
	}
	
	public synchronized void refresh(Updater updater){
		this.clear();
		updater.update();
		items = updater.getUpdatedList();
	}
	
	public ArrayList get(String name){
		return items.get(name);
	}
	
	public synchronized void clear(){
		items.clear();
		
	}

		
}