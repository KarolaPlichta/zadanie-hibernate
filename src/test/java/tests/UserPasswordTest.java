package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Checkerrules.UserPasswordRule;
import checker.CheckResult;
import checker.RuleResult;
import domain.User;

public class UserPasswordTest {
	
	UserPasswordRule rule = new UserPasswordRule();

	@Test
	public void checker_should_check_if_the_user_password_is_not_null(){
		User u = new User();
		CheckResult result =rule.checkRule(u);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_user_password_is_not_empty(){
		User u = new User();
		u.setLogin("");
		CheckResult result =rule.checkRule(u);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void chechker_should_check_if_the_password_is_not_too_easy(){
		User u = new User();
		u.setPassword("abcdef");
		CheckResult result =rule.checkRule(u);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	@Test
	public void chechker_should_check_if_the_password_is_not_too_short(){
		User u = new User();
		u.setPassword("mam3");
		CheckResult result =rule.checkRule(u);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	@Test
	public void chechker_should_return_Ok_if_the_password_is_correct(){
		User u = new User();
		u.setPassword("sdgdf6545");
		CheckResult result =rule.checkRule(u);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}


}
