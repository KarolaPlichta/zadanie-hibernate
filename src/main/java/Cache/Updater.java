package Cache;

import java.util.ArrayList;
import java.util.Map;


import domain.Address;
import domain.EnumerationValue;
import domain.Person;
import domain.PhoneNumber;
import domain.User;
import Repository.RepositoryCatalog;


public class Updater implements Runnable{
	
	private int lifespam;
	private Map<String, ArrayList> c;
	private ArrayList<User> users;
	private ArrayList<PhoneNumber> numbers;
	private ArrayList<Person> people;
	private ArrayList<EnumerationValue> enumerations;
	private ArrayList<Address> addresses;
	private RepositoryCatalog repositoryCatalog;

	
	public Updater(int lifespam, RepositoryCatalog repo){
		this.lifespam = lifespam;
		this.repositoryCatalog = repo;
	}
	
	public Updater(){
		this.lifespam = 1000;
	}
	
	public void run() {
		while(true){
			try {
				System.out.println("Trying to update cache");
				update();
				Thread.sleep(this.lifespam);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void update(){
		this.addresses = (ArrayList<Address>) repositoryCatalog.addresses().selectAll();
		this.enumerations = (ArrayList<EnumerationValue>) repositoryCatalog.enumerations().selectAll();
		this.people = (ArrayList<Person>) repositoryCatalog.people().selectAll();
		this.numbers = (ArrayList<PhoneNumber>) repositoryCatalog.phoneNumbers().selectAll();
		this.users = (ArrayList<User>) repositoryCatalog.users().selectAll();
	
	}


	public Map<String, ArrayList> getUpdatedList() {
		c.put("users", this.users);
		c.put("numbers", this.numbers);
		c.put("people", this.people);
		c.put("enumerations", this.enumerations);
		c.put("addresses", this.addresses);
		
		return c;
		
	}


}
