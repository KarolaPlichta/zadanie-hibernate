package domain;

import java.io.Serializable;

import javax.persistence.*;

@javax.persistence.Entity(name = "RolesPermissions")
public class RolesPermissions extends Entity implements Serializable{

	private static final long serialVersionUID = 1L;
	@Column(name="roleId", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int roleId;
	private int permissionId;


	public RolesPermissions(){
		
	}
	
	public RolesPermissions(int roleId, int permissionId) {
		super();
		this.roleId = roleId;
		this.permissionId = permissionId;
	}
	public int getRoleId() {
		return roleId;
	}
	public int getPermissionId() {
		return permissionId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}
}

	
	