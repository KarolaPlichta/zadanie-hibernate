package Repository;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import UnitOfWork.UnitOfWork;
import UnitOfWork.UnitOfWorkRepository;
import domain.Entity;
import domain.Person;

public class HsqlPersonRepository implements PersonRepository, UnitOfWorkRepository{
	
	private Query query;
	private Session session;
	private UnitOfWork unitOfWork;

	public HsqlPersonRepository(Session session, UnitOfWork unitOfWork) {
		this.session = session;
		this.unitOfWork = unitOfWork;
	}
	
	public Person withId(int id) {
		Person person = new Person();
		person = session.get(Person.class, id);
		return person;
	
	}
	@SuppressWarnings("unchecked")
	public List<Person> selectAll() {
		String selectAll = "FROM person ";
        query=session.createQuery(selectAll);
        return query.list();
	}

	public void add(Person entity) {
		 session.save(entity);
		
	}

	public void delete(Person entity) {
		session.delete(entity);
		
	}

	public void modify(Person entity) {
		 session.update(entity);
		
	}

	public int count() {
		int countPerson =0;
		countPerson = selectAll().size();
		return countPerson;
	}


	public Person withPesel(int pesel) {
		Person person = new Person();
		person = session.get(Person.class, pesel);
		return person;
	
	}

	@SuppressWarnings("unchecked")
	public List<Person> withSurname(String surname) {
		String withSurname = "FROM person WHERE surname = :surname";
        query=session.createQuery(withSurname);
        query.setParameter("surname",surname);
        return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Person> withSurnameAndName(String surname, String name) {
		String withSurnameAndName = "FROM person WHERE surname = :surname AND name =: name";
        query=session.createQuery(withSurnameAndName);
        query.setParameter("surname",surname);
        query.setParameter("name",name);
        return query.list();
	}
	
	public void persistAdd(Entity entity) {
		unitOfWork.markAsNew(entity, this);
	}

	public void persistDelete(Entity entity) {
		unitOfWork.markAsDeleted(entity, this);
	}

	public void persistUpdate(Entity entity) {
		unitOfWork.markAsChanged(entity, this);
		
	}


}
