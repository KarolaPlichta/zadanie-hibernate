package Repository;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import UnitOfWork.UnitOfWork;
import UnitOfWork.UnitOfWorkRepository;
import domain.Entity;
import domain.User;

public class HsqlUsersRepository  implements UserRepository, UnitOfWorkRepository{
	
	  private Query query;
	  private Session session;
	  private UnitOfWork unitOfWork;
	
	public HsqlUsersRepository(Session session, UnitOfWork unitOfWork){
		this.session = session;
		this.unitOfWork = unitOfWork;
		
	}
	

	public User withId(int id) {
		User user = new User();
		user = session.get(User.class, id);
	    return user;
		
	}

	@SuppressWarnings("unchecked")
	public List<User> selectAll() {
		String selectAll = "FROM user ";
        query=session.createQuery(selectAll);
        return query.list();
	}

	public void add(User entity) {
		 session.save(entity);
	}

	public void delete(User entity) {
		session.delete(entity);
	}

	public void modify(User entity) {
		session.update(entity);
	}

	public int count() {
		int countUser =0;
		countUser = selectAll().size();
		return countUser;
		
	}

	@SuppressWarnings("unchecked")
	public List<User> withLogin(String login) {
		String withLogin = "FROM user WHERE login = :login";
        query=session.createQuery(withLogin);
        query.setParameter("login",login);
        return query.list();
	}

	public User withLogginAndPassword(String login, String password) {
		String withLoginandPassword = "FROM user WHERE login = :login AND password = : password";
        query=session.createQuery(withLoginandPassword);
        query.setParameter("login",login);
        query.setParameter("password",password);
        return (User) query.list();
	}
	
	public void persistAdd(Entity entity) {
		unitOfWork.markAsNew(entity, this);
	}

	public void persistDelete(Entity entity) {
		unitOfWork.markAsDeleted(entity, this);
	}

	public void persistUpdate(Entity entity) {
		unitOfWork.markAsChanged(entity, this);
		
	}

		
	}

