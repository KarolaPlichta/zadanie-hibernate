package domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@javax.persistence.Entity(name = "Person")
public class Person extends Entity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="personId", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int personId;

	
	@OneToOne	
	@JoinColumn(name="userId")
		private User user;

	@OneToMany	
	@JoinColumn(name="addressId")
		private List<Address> address;
	@OneToMany	
	@JoinColumn(name="numberPhoneId")
		private List<PhoneNumber> numberPhone;
	
	private String firstName;
	private String surname;
	private String pesel;
	private String nip;
	private String email;
	private Date dateOfBirth;
	
	
	public Person(){
		
	}
	
	public Person(int personId, String firstName, String surname, String pesel, String nip, String email, Date dateOfBirth,
		User user, List<Address> address, List<PhoneNumber> numberPhone) {
	super();
	this.personId = personId;
	this.firstName = firstName;
	this.surname = surname;
	this.pesel = pesel;
	this.nip = nip;
	this.email = email;
	this.dateOfBirth = dateOfBirth;
	this.user = user;
	this.address = address;
	this.numberPhone = numberPhone;
}

	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

}
