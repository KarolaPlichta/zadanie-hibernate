package Repository;


public interface RepositoryCatalog {
	public EnumerationValueRepository enumerations();
	public UserRepository users();
	public AddressRepository addresses();
	public PhoneNumberRepository phoneNumbers();
	public PersonRepository people();
}
