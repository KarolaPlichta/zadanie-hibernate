package domain;

import java.io.Serializable;

import javax.persistence.*;

@javax.persistence.Entity(name = "EnumerationValue")
public class EnumerationValue extends Entity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="enumerationvalueid", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id; 	
	
	private int intKey;
	private String stringKey;
	private String value;
	private String enumerationName;
	
	public EnumerationValue(){
		
	}
	
	public EnumerationValue(int id, int intKey, String stringKey, String value, String enumerationName) {
		super();
		this.id = id;
		this.intKey = intKey;
		this.stringKey = stringKey;
		this.value = value;
		this.enumerationName = enumerationName;
	}
	
	
	public int getIntKey() {
		return intKey;
	}
	public void setIntKey(int intKey) {
		this.intKey = intKey;
	}
	public String getStringKey() {
		return stringKey;
	}
	public void setStringKey(String stringKey) {
		this.stringKey = stringKey;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getEnumerationName() {
		return enumerationName;
	}
	public void setEnumerationName(String enumerationName) {
		this.enumerationName = enumerationName;
	}
	


}
