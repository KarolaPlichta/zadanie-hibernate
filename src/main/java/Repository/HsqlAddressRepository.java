package Repository;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import UnitOfWork.UnitOfWork;
import UnitOfWork.UnitOfWorkRepository;
import domain.Address;
import domain.Entity;

public class HsqlAddressRepository implements AddressRepository, UnitOfWorkRepository{
	
	private Query query;
	private Session session;
	private UnitOfWork unitOfWork;

	protected HsqlAddressRepository(Session session, UnitOfWork unitOfWork) {
		this.session = session;
		this.unitOfWork = unitOfWork;

	}

	public Address withId(int id) {
		Address address = new Address();
		address = session.get(Address.class, id);
		return address;
	}
	@SuppressWarnings("unchecked")
	public List<Address> selectAll() {
		String selectAll = "FROM address ";
        query=session.createQuery(selectAll);
        return query.list();
	}
	
	public void add(Address entity) {
		session.save(entity);
	}

	public void delete(Address entity) {
		session.delete(entity);
		
	}

	public void modify(Address entity) {
		session.update(entity);

	}

	public int count() {
		int countAddress = 0;
		countAddress = selectAll().size();
		return countAddress;
	}

	@SuppressWarnings("unchecked")
	public List<Address> withCountryId(int id) {
		String withCountryId = "FROM Address WHERE countryId = :id";
        query=session.createQuery(withCountryId);
        query.setParameter("id",id);
        return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Address> withZipCode(String zipCode) {
		String withZipCode = "FROM Address WHERE zipCode = :zipCode";
        query=session.createQuery(withZipCode);
        query.setParameter("zipCode", zipCode);
        return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Address> withCountryIdAndCity(int id, String city) {
		String withCountryIdAndCity = "FROM Address WHERE countryId = :id AND city =: city";
        query=session.createQuery(withCountryIdAndCity);
        query.setParameter("id",id);
        query.setParameter("city",city);
        return query.list();
	}
	
	public void persistAdd(Entity entity) {
		unitOfWork.markAsNew(entity, this);
	}

	public void persistDelete(Entity entity) {
		unitOfWork.markAsDeleted(entity, this);
	}

	public void persistUpdate(Entity entity) {
		unitOfWork.markAsChanged(entity, this);
		
	}

	
}
