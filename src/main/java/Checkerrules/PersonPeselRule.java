package Checkerrules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Person;

public class PersonPeselRule implements ICanCheckRule<Person>{

	public CheckResult checkRule(Person entity) {
		
		if(entity.getPesel()==null){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getPesel().equals("")){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getPesel().length() != 11){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(CountPeselFigure(entity)==false){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		
		return new CheckResult("Fine", RuleResult.Ok);
	}
	
	private boolean CountPeselFigure(Person entity){
		
		int[] scales = {1, 3, 7, 9, 1, 3, 7 ,9 ,1 ,3};
		int sum = 0;
		
		for (int i = 0; i < 10; i++){
		sum += Integer.parseInt(entity.getPesel().substring(i, i+1)) * scales[i];
		System.out.println(sum);
		}
		int controlFigure = Integer.parseInt(entity.getPesel().substring(10, 11));
		sum %= 10;
		sum = 10 - sum;
		
		return (sum == controlFigure);
		
		}
	
}
