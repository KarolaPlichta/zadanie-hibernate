package Checkerrules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Person;

public class PersonEmailRule implements ICanCheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		
		if(entity.getEmail() == null){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getEmail().equals("")){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getEmail().contains("@")==false){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getEmail().contains(".")==false){
			return new CheckResult("Wrong", RuleResult.Error);
		}
	
			return new CheckResult("Fine", RuleResult.Ok);
	}

}
