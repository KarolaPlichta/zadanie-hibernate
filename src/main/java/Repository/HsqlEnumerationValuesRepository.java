package Repository;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import UnitOfWork.UnitOfWork;
import UnitOfWork.UnitOfWorkRepository;
import domain.Entity;
import domain.EnumerationValue;

public class HsqlEnumerationValuesRepository implements EnumerationValueRepository, UnitOfWorkRepository{
	
	private Query query;
	private Session session;
	private UnitOfWork unitOfWork;
	
	public HsqlEnumerationValuesRepository(Session session, UnitOfWork unitOfWork){
		this.session = session;
		this.unitOfWork = unitOfWork;
	}

	public EnumerationValue withId(int id) {
		  EnumerationValue enumerationValue = new EnumerationValue();
		  enumerationValue = session.get(EnumerationValue.class, id);
		return enumerationValue;
	}
	
	@SuppressWarnings("unchecked")
	public List<EnumerationValue> selectAll() {
		String selectAll = "FROM enumerationvalue ";
        query=session.createQuery(selectAll);
        return query.list();
	}

	public void add(EnumerationValue entity) {
		 session.save(entity);
	}

	public void delete(EnumerationValue entity) {
		session.delete(entity);
		
	}

	public void modify(EnumerationValue entity) {
		session.update(entity);

	}

	public int count() {
		int countEnumeration = 0;
		countEnumeration = selectAll().size();
		return countEnumeration;
	}

	@SuppressWarnings("unchecked")
	public List<EnumerationValue> withName(String name) {
		String withName = "FROM enumeration value WHERE name = :name";
        query=session.createQuery(withName);
        query.setParameter("name",name);
        return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<EnumerationValue> withIntKey(int key) {
		String withIntKey = "FROM enumeration value WHERE intKey = :intKey";
        query=session.createQuery(withIntKey);
        query.setParameter("intKey",key);
        return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<EnumerationValue> withStringKey(String key) {
		String withStringKey = "FROM enumeration value WHERE stringKey = :stringKey";
        query=session.createQuery(withStringKey);
        query.setParameter("stringKey",key);
        return query.list();
	}
	
	public void persistAdd(Entity entity) {
		unitOfWork.markAsNew(entity, this);
	}

	public void persistDelete(Entity entity) {
		unitOfWork.markAsDeleted(entity, this);
	}

	public void persistUpdate(Entity entity) {
		unitOfWork.markAsChanged(entity, this);
		
	}


	
}
