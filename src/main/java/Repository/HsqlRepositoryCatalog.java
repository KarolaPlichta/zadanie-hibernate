package Repository;

import org.hibernate.Session;

import UnitOfWork.UnitOfWork;

public class HsqlRepositoryCatalog implements RepositoryCatalog {
	
	private Session session;
	private UnitOfWork unitOfWork;
	
	public HsqlRepositoryCatalog(Session session, UnitOfWork unitOfWork){
		this.session = session;
		this.unitOfWork = unitOfWork;
	}
	
		public EnumerationValueRepository enumerations() {
			return new HsqlEnumerationValuesRepository(this.session, unitOfWork);
		}
	
		public UserRepository users() {
			return new HsqlUsersRepository(this.session, unitOfWork);
		}
		
		public AddressRepository addresses(){
			return new HsqlAddressRepository(session, unitOfWork);
		}


		public PhoneNumberRepository phoneNumbers() {
			return new HsqlPhoneNumberRepository(session, unitOfWork);
		}

		public PersonRepository people() {
			return new HsqlPersonRepository(session, unitOfWork);
		}

}
