package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Checkerrules.PersonPeselRule;
import checker.CheckResult;
import checker.RuleResult;
import domain.Person;

public class PersonPeselTest {
	
	PersonPeselRule rule = new PersonPeselRule();

	@Test
	public void checker_should_check_if_the_person_pesel_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_pesel_is_not_empty(){
		Person p = new Person();
		p.setPesel("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_pesel_lenght_is_11(){
		Person p = new Person();
		p.setPesel("2353454");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_ok_if_the_pesel_is_incorrect(){
		Person p = new Person();
		p.setPesel("85032208456");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	@Test
	public void checker_should_return_ok_if_the_pesel_is_correct(){
		Person p = new Person();
		p.setPesel("85032208451");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}
	

}
