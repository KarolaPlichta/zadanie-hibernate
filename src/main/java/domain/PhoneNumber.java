package domain;
import java.io.Serializable;

import javax.persistence.*;

@javax.persistence.Entity(name = "PhoneNumber")
public class PhoneNumber extends Entity  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Column(name="userid", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String phoneNumberId;
	
	@ManyToOne
	@JoinColumn(name="personId")
	private Person person;
	
	private String countryPrefix;
	private String cityPrefix;
	private String number;
	private int typeId;
	
	public PhoneNumber(){
		
	}
	
	
	public PhoneNumber(String phoneNumberId, String countryPrefix, String cityPrefix, String number, int typeId, Person person) {
		super();
		this.phoneNumberId = phoneNumberId;
		this.countryPrefix = countryPrefix;
		this.cityPrefix = cityPrefix;
		this.number = number;
		this.typeId = typeId;
		this.person = person;
	}
	
	
	public String getCountryPrefix() {
		return countryPrefix;
	}
	public void setCountryPrefix(String countryPrefix) {
		this.countryPrefix = countryPrefix;
	}
	public String getCityPrefix() {
		return cityPrefix;
	}
	public void setCityPrefix(String cityPrefix) {
		this.cityPrefix = cityPrefix;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

}
