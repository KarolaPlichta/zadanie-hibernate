package Repository;

import java.util.List;

import domain.PhoneNumber;

public interface PhoneNumberRepository extends IRepository<PhoneNumber> {
	
	public List<PhoneNumber> withCountryPrefix(String prefix);
	public PhoneNumber withNumber(String number);
	public List<PhoneNumber> withCountryAndCityPrefix(String countryPrefix, String cityPrefix);

}
