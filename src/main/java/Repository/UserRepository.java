package Repository;

import java.util.List;

import domain.User;

public interface UserRepository extends IRepository<User>{
	public List<User> withLogin(String login);
	public User withLogginAndPassword(String login, String password);

}
