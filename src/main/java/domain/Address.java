package domain;
import java.io.Serializable;

import javax.persistence.*;

@javax.persistence.Entity(name = "Address")
public class Address extends Entity implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(name="addessid", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int addressId;
	
	private int countryId;
	private int regionId;
	private String city;
	private String street;
	private String houseNumber;
	private String localNumber;
	private String zipCode;
	private int typeId;
	
	@OneToOne
	@JoinColumn(name="personId")
	private Person person;

	
	public Address(){
		
	}
	
	public Address(int addressId, int countryId, int regionId, String city, String street, String houseNumber,
			String localNumber, String zipCode, int typeId, Person person) {
		super();
		this.addressId = addressId;
		this.countryId = countryId;
		this.regionId = regionId;
		this.city = city;
		this.street = street;
		this.houseNumber = houseNumber;
		this.localNumber = localNumber;
		this.zipCode = zipCode;
		this.typeId = typeId;
		this.person = person;
	}
	
	
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public int getRegionId() {
		return regionId;
	}
	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getLocalNumber() {
		return localNumber;
	}
	public void setLocalNumber(String localNumber) {
		this.localNumber = localNumber;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

}
