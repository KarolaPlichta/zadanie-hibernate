package domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import Cache.Cache;
import Cache.Updater;
import Repository.HsqlRepositoryCatalog;
import Repository.RepositoryCatalog;
import UnitOfWork.HsqlUnitOfWork;
import UnitOfWork.UnitOfWork;

public class App 
{
    public static void main( String[] args )
    {

    	SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
    	UnitOfWork ouw = new HsqlUnitOfWork(session);
    	
    	EnumerationValue enumeration = new EnumerationValue();
    	enumeration.setEnumerationName("Nazwa");
    	enumeration.setIntKey(34);
    	enumeration.setStringKey("xx");
    	enumeration.setValue("Warotsc");
    	
    	
    	
    	RepositoryCatalog catalog = new HsqlRepositoryCatalog(session, ouw);
    
    	
    	Cache cache = Cache.getInstance();
    	Updater updater = new Updater(30000, catalog);
    	cache.refresh(updater);
    	
    	catalog.enumerations().add(enumeration);
    	
        session.close();
     	}
    }
