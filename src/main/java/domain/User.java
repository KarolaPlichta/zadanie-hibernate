package domain;

import java.io.Serializable;

import javax.persistence.*;

@javax.persistence.Entity(name = "User")
public class User extends Entity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="userid", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userId;
	
	@OneToMany
	@JoinColumn(name="permissionId")
	private RolesPermissions rolesPermissions;
	
	@OneToMany
	@JoinColumn(name="roleId")
	private UserRoles userRoles;

	@OneToOne
	@JoinColumn(name="personId")
	private Person person;
	

	private String login;
	private String password;
	
	public User(int userId, String login, String password, RolesPermissions rolesPermissions, UserRoles userRoles,
			Person person) {
		super();
		this.userId = userId;
		this.login = login;
		this.password = password;
		this.rolesPermissions = rolesPermissions;
		this.userRoles = userRoles;
		this.person = person;
	}
	
	public User(int userId, String login, String password, Person person) {
		super();
		this.userId = userId;
		this.login = login;
		this.password = password;
		this.person = person;
	}
	

	public User() {
		
	}

	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	

}
