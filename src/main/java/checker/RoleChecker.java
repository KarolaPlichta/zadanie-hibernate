package checker;

import java.util.ArrayList;
import java.util.List;

public class RoleChecker <TEntity> {

	private List<ICanCheckRule> rules ;

	public List<ICanCheckRule> getRules() {
		return rules;
	}

	public void setRules(List<ICanCheckRule> rules) {
		this.rules = rules;
	}
	
	public List<CheckResult> check(TEntity entity){
		List<CheckResult> result  = new ArrayList<CheckResult>();
		
		for(ICanCheckRule<TEntity> rule:rules){
			result.add(rule.checkRule(entity));
		}
		return result;
	}
}
