package Checkerrules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Person;

public class PersonNipRule implements ICanCheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		
		if(entity.getNip()==null){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getNip().equals("")){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getNip().length() != 10){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		if(entity.getNip().equals("1234567890")){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		
		if(CountNipFigure(entity.getNip()) == false){
			return new CheckResult("Wrong", RuleResult.Error);
		}
		
		return new CheckResult("Fine", RuleResult.Ok);
	}
	
	private boolean CountNipFigure(String nip){
		
		int[] scales = {6, 5, 7, 2, 3, 4, 5, 6, 7};
		int sum = 0;
		
		for (int i = 0; i < 9; i++){
		sum += Integer.parseInt(nip.substring(i, i+1)) * scales[i];
		}
		int controlFigure = Integer.parseInt(nip.substring(9, 10));
		sum %= 11;
		

		return (sum == controlFigure);
		
		}

}
