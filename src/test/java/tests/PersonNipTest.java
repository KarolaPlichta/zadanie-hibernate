package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Checkerrules.PersonNipRule;
import checker.CheckResult;
import checker.RuleResult;
import domain.Person;

public class PersonNipTest {
	
	PersonNipRule rule = new PersonNipRule();

	@Test
	public void checker_should_check_if_the_Nip_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_Nip_is_not_empty(){
		Person p = new Person();
		p.setNip("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_Nip_lenght_is_not_10(){
		Person p = new Person();
		p.setNip("4592");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_Nip_is_incorrect(){
		Person p = new Person();
		p.setNip("1234567890");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_count_if_the_Nip_is_incorrect(){
		Person p = new Person();
		p.setNip("1234563216");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	@Test
	public void checker_should_count_if_the_Nip_is_correct(){
		Person p = new Person();
		p.setNip("1234563218");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	

}
