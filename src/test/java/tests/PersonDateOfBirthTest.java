package tests;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

import Checkerrules.PersonDateOfBirthRule;
import checker.CheckResult;
import checker.RuleResult;
import domain.Person;

public class PersonDateOfBirthTest {
	
	PersonDateOfBirthRule rule = new PersonDateOfBirthRule();
	DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

	@Test
	public void checker_should_check_if_the_person_dateOfBirth_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	

	@Test
	public void checker_should_check_if_the_person_dateOfBirth_match_to_pesel() throws ParseException{
		Person p = new Person();
		p.setDateOfBirth(dateFormat.parse("19951103"));
		p.setPesel("94100324598");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}


	@Test
	public void checker_should_return_OK_if_the_dateOfBirth_match_to_pesel() throws ParseException{
		Person p = new Person();
		p.setDateOfBirth(dateFormat.parse("19951118"));
		p.setPesel("95111803602");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}
	
	

	
	
}
