package domain;

public abstract class Entity {
	
	private int Id;
	private EntityState state;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public EntityState getState() {
		return state;
	}
	public void setState(EntityState state) {
		this.state = state;
	}

}
